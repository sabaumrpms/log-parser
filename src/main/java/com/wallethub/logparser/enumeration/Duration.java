package com.wallethub.logparser.enumeration;

import com.wallethub.logparser.exception.InvalidArgumentException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public enum Duration {

    HOURLY {
        @Override
        public LocalDateTime calculateEndDate(LocalDateTime startDate) {
            return startDate.plus(1, ChronoUnit.HOURS).minusNanos(1);
        }
    },
    DAILY {
        @Override
        public LocalDateTime calculateEndDate(LocalDateTime startDate) {
            return startDate.plus(1, ChronoUnit.DAYS).minusNanos(1);
        }
    };

    public static Duration fromValue(String value) {
        for (Duration duration : values()) {
            if (duration.name().equalsIgnoreCase(value)) {
                return duration;
            }
        }
        throw new InvalidArgumentException(String.format("Parâmetro duration inválido: '%s'", value));
    }

    public abstract LocalDateTime calculateEndDate(LocalDateTime startDate);
}
