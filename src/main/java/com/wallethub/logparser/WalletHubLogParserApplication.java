package com.wallethub.logparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalletHubLogParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalletHubLogParserApplication.class, args);
    }

}
