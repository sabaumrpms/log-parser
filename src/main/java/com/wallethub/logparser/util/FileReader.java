package com.wallethub.logparser.util;

import com.wallethub.logparser.exception.InvalidFileException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;
import org.springframework.stereotype.Component;

@Component
public class FileReader {

    private Scanner scanner;

    public void readFile(Path filePath) {
        try {
            scanner = new Scanner(filePath);
        } catch (IOException e) {
            throw new InvalidFileException(String.format("Erro ao ler arquivo '%s'", filePath), e);
        }
    }

    public String getNextLine() {
        return scanner.nextLine();
    }

    public boolean hasMoreLinesToRead() {
        return scanner.hasNext();
    }

}
