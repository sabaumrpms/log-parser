package com.wallethub.logparser.util;

import com.wallethub.logparser.engine.Argument;
import com.wallethub.logparser.exception.InvalidArgumentException;
import java.util.List;
import java.util.Optional;

public enum ArgumentType {

    ACCESS_LOG("accesslog"),
    START_DATE("startDate"),
    DURATION("duration"),
    THRESHOLD("threshold");

    private String key;

    ArgumentType(String key) {
        this.key = key;
    }

    public static ArgumentType of(String key) {
        for (ArgumentType argumentType : values()) {
            if (argumentType.key.equals(key)) {
                return argumentType;
            }
        }
        throw new InvalidArgumentException(String.format("Invalid argument for key '%s'", key));
    }

    public Optional<Argument> retrieveFromList(List<Argument> arguments) {
        for (Argument argument : arguments) {
            if (this == argument.getArgumentType()) {
                return Optional.of(argument);
            }
        }
        return Optional.empty();
    }

    public String getKey() {
        return key;
    }
}
