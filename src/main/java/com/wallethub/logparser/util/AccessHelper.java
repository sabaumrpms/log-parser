package com.wallethub.logparser.util;

import com.wallethub.logparser.engine.Argument;
import com.wallethub.logparser.enumeration.Duration;
import com.wallethub.logparser.exception.InvalidLineException;
import com.wallethub.logparser.model.Access;
import com.wallethub.logparser.repository.AccessRepository;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class AccessHelper {

    private static final Pattern LINE_PATTERN = Pattern.compile("([\\d-:. ]*)\\|([\\d.]*)\\|.*");
    private static final DateTimeFormatter DATE_ARG_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");

    private final AccessRepository accessRepository;
    private final FileReader fileReader;

    public AccessHelper(AccessRepository accessRepository, FileReader fileReader) {
        this.accessRepository = accessRepository;
        this.fileReader = fileReader;
    }

    public void populate(List<Access> accesses) {
        if (CollectionUtils.isNotEmpty(accesses)) {
            accessRepository.saveAll(accesses);
        }
    }

    public List<Access> extractAccesses(List<Argument> arguments) {
        List<Access> requests = new ArrayList<>();
        Optional<Argument> accessLogArgument = ArgumentType.ACCESS_LOG.retrieveFromList(arguments);
        if (accessLogArgument.isPresent()) {
            Argument accessLog = accessLogArgument.get();
            fileReader.readFile(Paths.get(accessLog.getValue()));
            while (fileReader.hasMoreLinesToRead()) {
                requests.add(generateRequest(fileReader.getNextLine()));
            }
        }
        return requests;
    }

    private Access generateRequest(String line) {
        Matcher matcher = LINE_PATTERN.matcher(line);
        if (matcher.find()) {
            String stringDate = matcher.group(1);
            if (StringUtils.isNotBlank(stringDate)) {
                return new Access(fileDateToLocalDateTime(stringDate), matcher.group(2));
            }
        }
        throw new InvalidLineException(String.format("Linha fora do padrão esperado: %s", line));
    }

    private LocalDateTime argDateToLocalDateTime(String stringDate) {
        return LocalDateTime.from(DATE_ARG_FORMATTER.parse(stringDate));
    }

    private LocalDateTime fileDateToLocalDateTime(String stringDate) {
        String[] splittedDate = stringDate.split(" ");
        LocalDate localDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(splittedDate[0]));
        LocalTime localTime = LocalTime.from(DateTimeFormatter.ISO_LOCAL_TIME.parse(splittedDate[1]));
        return localDate.atTime(localTime);
    }

    public List<Access> filter(String date, String sDuration, String threshold) {
        LocalDateTime startDate = argDateToLocalDateTime(date);
        Duration duration = Duration.fromValue(sDuration);
        return accessRepository.findRequestsByDatesAndThreshold(startDate, duration.calculateEndDate(startDate), Long.valueOf(threshold));
    }
}
