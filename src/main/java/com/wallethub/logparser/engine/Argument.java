package com.wallethub.logparser.engine;

import com.wallethub.logparser.util.ArgumentType;

public class Argument {

    private ArgumentType argumentType;
    private String value;

    Argument(ArgumentType argumentType, String value) {
        this.argumentType = argumentType;
        this.value = value;
    }

    public ArgumentType getArgumentType() {
        return argumentType;
    }

    public String getValue() {
        return value;
    }
}
