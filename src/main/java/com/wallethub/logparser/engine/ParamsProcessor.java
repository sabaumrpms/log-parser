package com.wallethub.logparser.engine;

import static com.wallethub.logparser.model.BlockedIp.REASON_TEMPLATE;

import com.wallethub.logparser.exception.InvalidArgumentException;
import com.wallethub.logparser.model.Access;
import com.wallethub.logparser.model.BlockedIp;
import com.wallethub.logparser.repository.BlockedIpRepository;
import com.wallethub.logparser.util.AccessHelper;
import com.wallethub.logparser.util.ArgumentType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ParamsProcessor implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(ParamsProcessor.class);

    private final AccessHelper accessHelper;
    private final BlockedIpRepository blockedIpRepository;

    public ParamsProcessor(AccessHelper accessHelper, BlockedIpRepository blockedIpRepository) {
        this.accessHelper = accessHelper;
        this.blockedIpRepository = blockedIpRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        logger.info("Your application started with option names : {}", args.getOptionNames());

        List<Argument> arguments = resolveArgs(args);
        populateDatabase(arguments);
        ValidatedArguments validatedArguments = validateArguments(arguments);
        List<Access> accesses = applyRules(validatedArguments);
        blockIps(accesses, validatedArguments);
    }

    private ValidatedArguments validateArguments(List<Argument> arguments) {
        Optional<Argument> startDate = ArgumentType.START_DATE.retrieveFromList(arguments);
        Optional<Argument> duration = ArgumentType.DURATION.retrieveFromList(arguments);
        Optional<Argument> threshold = ArgumentType.THRESHOLD.retrieveFromList(arguments);

        if (!startDate.isPresent()) {
            throw new InvalidArgumentException("Error passing parameters: startDate was not found.");
        }
        if (!duration.isPresent()) {
            throw new InvalidArgumentException("Error passing parameters: duration was not found.");
        }
        if (!threshold.isPresent()) {
            throw new InvalidArgumentException("Error passing parameters: threshold was not found.");
        }

        return new ValidatedArguments(startDate.get(), duration.get(), threshold.get());
    }

    private void populateDatabase(List<Argument> arguments) {
        List<Access> accesses = accessHelper.extractAccesses(arguments);
        accessHelper.populate(accesses);
    }

    private List<Access> applyRules(ValidatedArguments validatedArguments) {
        return accessHelper.filter(validatedArguments.getStartDate(), validatedArguments.getDuration(), validatedArguments.getThreshold());
    }

    private void blockIps(List<Access> accesses, ValidatedArguments arguments) {
        if (CollectionUtils.isNotEmpty(accesses)) {
            blockedIpRepository.saveAll(generateBlockedIps(accesses, arguments));
        }
    }

    private List<BlockedIp> generateBlockedIps(List<Access> accesses, ValidatedArguments arguments) {
        List<BlockedIp> blockedIps = new ArrayList<>();
        for (Access access : accesses) {
            blockedIps.add(generateBlockedIp(access, arguments));
        }
        return blockedIps;
    }

    private BlockedIp generateBlockedIp(Access access, ValidatedArguments arguments) {
        String reason = String.format(REASON_TEMPLATE, arguments.getDuration(), arguments.getStartDate(), arguments.getThreshold(), access.getRequests());
        System.out.println(String.format("Blocking the IP '%s' for this reason: %s", access.getIp(), reason));
        return new BlockedIp(access.getIp(), reason);
    }

    private List<Argument> resolveArgs(ApplicationArguments args) {
        List<Argument> arguments = new ArrayList<>();
        for (String argKey : args.getOptionNames()) {
            String argValue = extractArgumentValue(args, argKey);
            arguments.add(new Argument(ArgumentType.of(argKey), argValue));
        }

        return arguments;
    }

    private String extractArgumentValue(ApplicationArguments args, String argKey) {
        List<String> argValue = args.getOptionValues(argKey);
        if (CollectionUtils.isEmpty(argValue)) {
            throw new InvalidArgumentException(String.format("No value for argument '%s'", argKey));
        }
        return argValue.get(0);
    }

    private class ValidatedArguments {
        private Argument startDate;
        private Argument duration;
        private Argument threshold;

        private ValidatedArguments(Argument startDate, Argument duration, Argument threshold) {
            this.startDate = startDate;
            this.duration = duration;
            this.threshold = threshold;
        }

        private String getStartDate() {
            return startDate.getValue();
        }

        private String getDuration() {
            return duration.getValue();
        }

        private String getThreshold() {
            return threshold.getValue();
        }
    }
}
