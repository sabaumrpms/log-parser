package com.wallethub.logparser.repository;

import com.wallethub.logparser.model.Access;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AccessRepository extends CrudRepository<Access, Long> {

    @Query("SELECT new Access(ip, count(1) as requests) FROM Access WHERE date BETWEEN :startDate AND :endDate GROUP BY ip HAVING count(1) > :threshold")
    List<Access> findRequestsByDatesAndThreshold(LocalDateTime startDate, LocalDateTime endDate, Long threshold);

}
