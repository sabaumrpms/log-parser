package com.wallethub.logparser.repository;

import com.wallethub.logparser.model.BlockedIp;
import org.springframework.data.repository.CrudRepository;

public interface BlockedIpRepository extends CrudRepository<BlockedIp, Long> {

}
