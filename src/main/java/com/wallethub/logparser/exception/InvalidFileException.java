package com.wallethub.logparser.exception;

public class InvalidFileException extends RuntimeException {

    public InvalidFileException(String message, Throwable t) {
        super(message, t);
    }

}
