package com.wallethub.logparser.exception;

public class InvalidLineException extends RuntimeException {

    public InvalidLineException(String message) {
        super(message);
    }

}
