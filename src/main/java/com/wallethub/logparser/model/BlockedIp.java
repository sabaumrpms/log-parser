package com.wallethub.logparser.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class BlockedIp {

    public static final String REASON_TEMPLATE = "Blocked by rule '%s', starting at '%s', respecting the threshold '%s'. Number of requests: %s";

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "ip", length = 16, nullable = false)
    private String ip;

    @Column(name = "reason", length = 200, nullable = false)
    private String reason;


    public BlockedIp(String ip, String reason) {
        this.ip = ip;
        this.reason = reason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
