package com.wallethub.logparser;

import static org.mockito.ArgumentMatchers.anyString;

import com.wallethub.logparser.engine.MockArgument;
import com.wallethub.logparser.engine.ParamsProcessor;
import com.wallethub.logparser.exception.InvalidArgumentException;
import com.wallethub.logparser.repository.BlockedIpRepository;
import com.wallethub.logparser.util.AccessHelper;
import com.wallethub.logparser.util.MockUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.DefaultApplicationArguments;

@RunWith(MockitoJUnitRunner.class)
public class ParamsProcessorTest {

    @Mock
    private AccessHelper accessHelper;
    @Mock
    private BlockedIpRepository blockedIpRepository;
    @InjectMocks
    private ParamsProcessor paramsProcessor;

    @Test
    public void run() {
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(MockArgument.FULL_FILTER_ARGS);
        paramsProcessor.run(fakeArgs);

        Mockito.verify(accessHelper).populate(Mockito.anyList());
    }

    @Test(expected = InvalidArgumentException.class)
    public void run_withNoValueArgument_shouldThrowException() {
        String[] args = { MockArgument.NO_VALUE_ARG };
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(args);
        paramsProcessor.run(fakeArgs);
    }

    @Test(expected = InvalidArgumentException.class)
    public void run_withInvalidArgument_shouldThrowException() {
        String[] args = {"--wrongArgument=wrongValue"};
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(args);
        paramsProcessor.run(fakeArgs);
    }

    @Test
    public void run_shouldCallAccessHelperFilter() {
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(MockArgument.FULL_FILTER_ARGS);

        paramsProcessor.run(fakeArgs);

        Mockito.verify(accessHelper).filter(MockArgument.VAL1, MockArgument.VAL2, MockArgument.VAL3);

    }

    @Test(expected = InvalidArgumentException.class)
    public void run_withoutStartDate_shouldThrowException() {
        String[] args = { MockArgument.FULL_ARG2, MockArgument.FULL_ARG3 };
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(args);

        paramsProcessor.run(fakeArgs);
    }

    @Test(expected = InvalidArgumentException.class)
    public void run_withoutDuration_shouldThrowException() {
        String[] args = { MockArgument.FULL_ARG1, MockArgument.FULL_ARG3 };
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(args);

        paramsProcessor.run(fakeArgs);
    }

    @Test(expected = InvalidArgumentException.class)
    public void run_withoutThreshold_shouldThrowException() {
        String[] args = { MockArgument.FULL_ARG1, MockArgument.FULL_ARG2 };
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(args);

        paramsProcessor.run(fakeArgs);
    }

    @Test
    public void run_shouldCallBlockedIpsSaveAll() {
        ApplicationArguments fakeArgs = new DefaultApplicationArguments(MockArgument.FULL_FILTER_ARGS);
        Mockito.when(accessHelper.filter(anyString(), anyString(), anyString())).thenReturn(MockUtils.accesses());

        paramsProcessor.run(fakeArgs);

        Mockito.verify(blockedIpRepository).saveAll(Mockito.anyList());
    }

}