package com.wallethub.logparser.util;

import com.wallethub.logparser.model.Access;
import java.util.ArrayList;
import java.util.List;

public class MockUtils {

    public static List<Access> accesses() {
        List<Access> accesses = new ArrayList<>();
        accesses.add(new Access());
        return accesses;
    }

}
