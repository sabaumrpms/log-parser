package com.wallethub.logparser.util;

import com.wallethub.logparser.exception.InvalidFileException;
import java.nio.file.Paths;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FileReaderTest {

    private static final String TST_FILE_PATH = "accessTst.log";
    private static final String FIRST_LINE = "2017-01-01 23:59:57.335|192.168.25.244|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36\"";

    @InjectMocks
    private FileReader fileReader;

    @Test
    public void hasMoreLinesToRead_shouldCallHasMoreLinesToRead() {
        fileReader.readFile(Paths.get(getClass().getClassLoader().getResource(TST_FILE_PATH).getFile()));
        Assert.assertTrue("Após ler arquivo, deve ter linhas para ler", fileReader.hasMoreLinesToRead());
    }

    @Test
    public void hasMoreLinesToRead_shouldCallGetNextLine() {
        fileReader.readFile(Paths.get(getClass().getClassLoader().getResource(TST_FILE_PATH).getFile()));
        Assert.assertEquals("Após ler arquivo, deve ler linha", FIRST_LINE, fileReader.getNextLine());
    }

    @Test(expected = InvalidFileException.class)
    public void readFile_invalidFilePath_shouldThrowException() {
        fileReader.readFile(Paths.get("/wrong/file/path.log"));
    }

}