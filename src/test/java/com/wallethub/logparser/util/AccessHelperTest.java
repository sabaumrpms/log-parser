package com.wallethub.logparser.util;

import static org.mockito.Mockito.times;

import com.wallethub.logparser.engine.MockArgument;
import com.wallethub.logparser.exception.InvalidArgumentException;
import com.wallethub.logparser.exception.InvalidLineException;
import com.wallethub.logparser.model.Access;
import com.wallethub.logparser.repository.AccessRepository;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AccessHelperTest {

    @Mock
    private AccessRepository repository;
    @Mock
    private FileReader fileReader;
    @InjectMocks
    private AccessHelper accessHelper;

    @Test
    public void populate_shouldCallRepositoryPopulate() {
        List<Access> accesses = MockArgument.accesses();
        accessHelper.populate(accesses);

        Mockito.verify(repository).saveAll(Mockito.anyList());
    }

    @Test
    public void populate_withNoAccess_shouldDoNothing() {
        accessHelper.populate(new ArrayList<>());
        Mockito.verify(repository, times(0)).saveAll(Mockito.anyList());
    }

    @Test
    public void populate_withNullAccess_shouldDoNothing() {
        accessHelper.populate(null);
        Mockito.verify(repository, times(0)).saveAll(Mockito.anyList());
    }

    @Test
    public void extractRequests_withNoLogFileArgument_shouldDoNothing() {
        accessHelper.extractAccesses(Arrays.asList(MockArgument.validDuration()));

        Mockito.verify(fileReader, times(0)).readFile(Paths.get(MockArgument.ACCESS_LOG_FILE_PATH));
    }

    @Test
    public void extractRequests_shouldCallFileReader() {
        accessHelper.extractAccesses(MockArgument.validList());

        Mockito.verify(fileReader).readFile(Paths.get(MockArgument.ACCESS_LOG_FILE_PATH));
    }

    @Test
    public void extractRequests_shouldGetNextLineTwice() {
        Mockito.when(fileReader.hasMoreLinesToRead()).thenReturn(true, true, false);
        String line1 = "2017-01-01 23:59:58.791|192.168.229.98|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36\"";
        String line2 = "2017-01-01 23:59:59.608|192.168.122.135|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0\"";
        Mockito.when(fileReader.getNextLine()).thenReturn(line1, line2);

        accessHelper.extractAccesses(MockArgument.validList());

        Mockito.verify(fileReader, times(2)).getNextLine();
    }

    @Test
    public void extractRequests_shouldReturn2Requests() {
        Mockito.when(fileReader.hasMoreLinesToRead()).thenReturn(true, true, false);
        String line1 = "2017-01-01 23:59:58.791|192.168.229.98|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36\"";
        String line2 = "2017-01-01 23:59:59.608|192.168.122.135|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0\"";
        Mockito.when(fileReader.getNextLine()).thenReturn(line1, line2);

        List<Access> requests = accessHelper.extractAccesses(MockArgument.validList());

        Assert.assertEquals("Retorno deve conter 2 requests", 2, requests.size());
    }

    @Test
    public void extractRequests_shouldReturnRequestValid() {
        String line1 = "2017-01-01 23:59:58.791|192.168.229.98|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36\"";
        LocalDateTime expectedDate = LocalDate.of(2017, Month.JANUARY, 1).atTime(23, 59, 58).plus(791, ChronoUnit.MILLIS);
        Mockito.when(fileReader.hasMoreLinesToRead()).thenReturn(true, false);
        Mockito.when(fileReader.getNextLine()).thenReturn(line1);

        List<Access> requests = accessHelper.extractAccesses(MockArgument.validList());

        Assert.assertEquals("Primeiro request deve conter IP correto", "192.168.229.98", requests.get(0).getIp());
        Assert.assertEquals("Primeiro request deve conter data correta", expectedDate, requests.get(0).getDate());
    }

    @Test(expected = InvalidLineException.class)
    public void extractRequests_lineWithoutDate_shouldThrowException() {
        Mockito.when(fileReader.hasMoreLinesToRead()).thenReturn(true, false);
        String line1 = "192.168.229.98|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36\"";
        Mockito.when(fileReader.getNextLine()).thenReturn(line1);

        accessHelper.extractAccesses(MockArgument.validList());
    }

    @Test(expected = InvalidLineException.class)
    public void extractRequests_lineWithoutIp_shouldThrowException() {
        Mockito.when(fileReader.hasMoreLinesToRead()).thenReturn(true, false);
        String line1 = "2017-01-01 23:59:58.791|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36\"";
        Mockito.when(fileReader.getNextLine()).thenReturn(line1);

        accessHelper.extractAccesses(MockArgument.validList());
    }

    @Test
    public void filter_shouldCallRepositoryFindRequestsByDatesAndThreshold() {
        String sDate = MockArgument.VAL1;
        String sDuration = MockArgument.VAL2;
        String sThreshold = MockArgument.VAL3;
        LocalDateTime dateTime = LocalDate.of(2017, Month.JANUARY, 1).atTime(LocalTime.of(13, 0, 0));

        accessHelper.filter(sDate, sDuration, sThreshold);

        Mockito.verify(repository).findRequestsByDatesAndThreshold(dateTime, dateTime.plus(1, ChronoUnit.HOURS).minusNanos(1), 200L);
    }

    @Test(expected = InvalidArgumentException.class)
    public void filter_withInvalidDuration_shouldThrowException() {
        String sDate = MockArgument.VAL1;
        String sDuration = "invalidDuration";
        String sThreshold = MockArgument.VAL3;

        accessHelper.filter(sDate, sDuration, sThreshold);
    }
}