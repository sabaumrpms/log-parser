package com.wallethub.logparser.engine;

import com.wallethub.logparser.model.Access;
import com.wallethub.logparser.util.ArgumentType;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MockArgument {

    private static final String ARG1 = "startDate";
    private static final String ARG2 = "duration";
    private static final String ARG3 = "threshold";

    public static final String VAL1 = "2017-01-01.13:00:00";
    public static final String VAL2 = "hourly";
    public static final String VAL3 = "200";
    public static final String FULL_ARG1 = String.format("--%s=%s", ARG1, VAL1);
    public static final String FULL_ARG2 = String.format("--%s=%s", ARG2, VAL2);
    public static final String FULL_ARG3 = String.format("--%s=%s", ARG3, VAL3);
    public static final String[] FULL_FILTER_ARGS = { FULL_ARG1, FULL_ARG2, FULL_ARG3 };
    public static final String NO_VALUE_ARG = String.format("--%s", ARG1);
    public static final String ACCESS_LOG_FILE_PATH = "~/accessLog.log";

    public static List<Access> accesses() {
        return Arrays.asList(access1(), access2());
    }

    private static Access access1() {
        return new Access(LocalDateTime.now(), "192.168.0.1");
    }

    private static Access access2() {
        return new Access(LocalDateTime.now(), "192.168.0.2");
    }

    public static List<Argument> validList() {
        return Arrays.asList(validDuration(), validLogFile());
    }

    public static Argument validDuration() {
        return new Argument(ArgumentType.DURATION, "200");
    }

    private static Argument validLogFile() {
        return new Argument(ArgumentType.ACCESS_LOG, ACCESS_LOG_FILE_PATH);
    }

}
